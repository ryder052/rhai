#pragma once
#include "stdafx.h"

#define DBG8(...) Debug::_debug_internal<8>(L##__VA_ARGS__)
#define DBG(...) Debug::_debug_internal<32>(L##__VA_ARGS__)
#define DBG64(...) Debug::_debug_internal<64>(L##__VA_ARGS__)
#define DBG128(...) Debug::_debug_internal<128>(L##__VA_ARGS__)

#define SAFE_DELETE(ptr) if(ptr) { delete ptr; ptr = nullptr; }
#define SAFE_DELETE_CONTAINER(cont) for(auto ptr : cont) SAFE_DELETE(ptr)
#define FIND(container, value) ( !##container##.empty() && ##container##.find(##value##) != ##container##.end() )

#define MAKE_PTR(type, ...) make_shared<##type##>(__VA_ARGS__)

namespace Debug
{
	template<int N>
	inline void _debug_internal(const WCHAR* szFormat, ...)
	{
		WCHAR szBuff[N];
		va_list arg;
		va_start(arg, szFormat);
		_vsnwprintf_s(szBuff, sizeof(szBuff), szFormat, arg);
		va_end(arg);

		OutputDebugString(szBuff);
	}
}

namespace Math
{
	inline UINT Power(UINT n)
	{
		return (n == 0) ? 1 : n*Power(n - 1);
	}

	inline UINT BinCoef(UINT n, UINT k)
	{
		return Power(n) / (Power(k)*Power(n - k));
	}
}

namespace Conversions
{
	// Method(param_count, ...) wrapper
	template<typename ObjType, typename ParamType, typename Func>
	inline auto CallVA(ObjType&& obj, Func func, const deque<ParamType>& params)
	{
		static ParamType p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
		static ParamType p11, p12, p13, p14, p15, p16, p17, p18, p19, p20;
		static ParamType p21, p22, p23, p24, p25, p26, p27, p28, p29, p30;
		static ParamType p31, p32, p33, p34, p35, p36, p37, p38, p39, p40;
		static ParamType p41, p42, p43, p44, p45, p46, p47, p48, p49, p50;
		static ParamType p51, p52, p53, p54, p55, p56, p57, p58, p59, p60;

		int S = params.size();

#define param_n(n) case n##: p##n = params[##n##-1]

		switch (S)
		{
			param_n(60); param_n(59); param_n(58); param_n(57); param_n(56); param_n(55); param_n(54); param_n(53); param_n(52); param_n(51);
			param_n(50); param_n(49); param_n(48); param_n(47); param_n(46); param_n(45); param_n(44); param_n(43); param_n(42); param_n(41);
			param_n(40); param_n(39); param_n(38); param_n(37); param_n(36); param_n(35); param_n(34); param_n(33); param_n(32); param_n(31);
			param_n(30); param_n(29); param_n(28); param_n(27); param_n(26); param_n(25); param_n(24); param_n(23); param_n(22); param_n(21);
			param_n(20); param_n(19); param_n(18); param_n(17); param_n(16); param_n(15); param_n(14); param_n(13); param_n(12); param_n(11);
			param_n(10); param_n(9);  param_n(8);  param_n(7);  param_n(6);  param_n(5);  param_n(4);  param_n(3);  param_n(2);  param_n(1);
		}

#undef param_n

		return (obj->*func)(S, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10,
			p11, p12, p13, p14, p15, p16, p17, p18, p19, p20,
			p21, p22, p23, p24, p25, p26, p27, p28, p29, p30,
			p31, p32, p33, p34, p35, p36, p37, p38, p39, p40,
			p41, p42, p43, p44, p45, p46, p47, p48, p49, p50,
			p51, p52, p53, p54, p55, p56, p57, p58, p59, p60);
	}

	template<typename Outer, typename Inner>
	inline vector<Inner> AsVector(const Outer& container)
	{
		vector<Inner> result;
		for (const auto& e : container)
			result.push_back(e.second);

		return result;
	}
}
