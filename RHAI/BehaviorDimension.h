#pragma once
#include "stdafx.h"
#include "BehaviorBindable.h"
#include "Behavior.h"

namespace RHAI
{
	struct Node;

	RHAI_CLASS(BehaviorDimension)
	{
		BehaviorDimension() = default;
		BehaviorDimension(pINodeBindable plus) { BindPositive(plus); }
		BehaviorDimension(pINodeBindable plus, pINodeBindable minus) { Bind(plus, minus); }
		~BehaviorDimension() = default;

		void Bind(pINodeBindable plus, pINodeBindable minus) { BindPositive(plus); BindNegative(minus); }
		void BindPositive(pINodeBindable plus) { m_Positive = plus; }
		void BindNegative(pINodeBindable minus) { m_Negative = minus; }

		RHAI_FIELD(pINodeBindable, Positive) = nullptr;
		RHAI_FIELD(pINodeBindable, Negative) = nullptr;
		RHAI_FIELD(bool, Enabled) = true;
	};
}