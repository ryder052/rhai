#pragma once
#include "BehaviorBindable.h"
#include "RDelegate.hpp"

namespace RHAI
{
	RHAI_CLASS(Behavior) : public INodeBindable
	{
		Behavior() = delete;
		Behavior(RDelegate<void, float> definition) : m_BehaviorDelegate(definition) {}

		void Perform(float intensity) const override;

		RHAI_FIELDT(RDelegate<void, float>, BehaviorDelegate);
	};
}
