#pragma once
using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GLOBALS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define RHAI_CLASS(name) \
	typedef std::shared_ptr<struct name> p##name; \
	struct name

#define RHAI_FIELD(type, name) \
	public: \
		const type##& Get##name##() { return m_##name##; } \
		void Set##name##(const type##& other) { m_##name = other; } \
	protected: \
		type m_##name

#define RHAI_FIELDT(ty, pe, name) \
	public: \
		const ty##,##pe##& Get##name##() { return m_##name##; } \
		void Set##name##(const ty##,##pe##& other) { m_##name = other; } \
	protected: \
		ty##,##pe m_##name

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
