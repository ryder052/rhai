#include "stdafx.h"
#include "Behavior.h"

using namespace RHAI;

void Behavior::Perform(float intensity) const
{
	m_BehaviorDelegate(intensity);
}
