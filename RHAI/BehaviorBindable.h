#pragma once

namespace RHAI
{
	RHAI_CLASS(INodeBindable)
	{
		INodeBindable() = default;
		virtual ~INodeBindable() = default;

		virtual void Perform(float intensity) const = 0;
	};
}
