#include "stdafx.h"
#include "Node.h"
#include "HelpersInternal.h"
#include "floatfann.h"
#include "fann_cpp.h"

using namespace RHAI;

UINT Node::AddDimension(pINodeBindable plus)
{
	m_Dimensions.push_back(make_shared<BehaviorDimension>(plus));
	m_CurrentDrive.push_back(0.0f);

	return m_Dimensions.size() - 1;
}

UINT Node::AddDimension(pINodeBindable plus, pINodeBindable minus)
{
	m_Dimensions.push_back(make_shared<BehaviorDimension>(plus, minus));
	m_CurrentDrive.push_back(0.0f);

	return m_Dimensions.size() - 1;
}

bool Node::AddAspect(const string& aspect_name)
{
	if (FIND(m_Aspects, aspect_name))
		return false;

	m_Aspects[aspect_name] = 0.0f;

	for (auto&& dimension : m_Dimensions)
	{
		if (auto node = dynamic_cast<Node*>(dimension->GetPositive().get()))
			node->AddAspect(aspect_name);
		if (auto node = dynamic_cast<Node*>(dimension->GetNegative().get()))
			node->AddAspect(aspect_name);
	}

	return true;
}

bool Node::Build(ENeuralPreset preset, shared_ptr<FANN::neural_net> preset_custom_data /*=nullptr*/)
{
	UINT in = m_Aspects.size();
	UINT out = m_Dimensions.size();

	if (in < out / 2)
		return false;

	m_NeuralNet = make_shared<FANN::neural_net>();

	switch (preset)
	{
		case ENeuralPreset::Simple:
		{
			m_NeuralNet->create_standard(3, in, roundf(0.67f*(in + out)), out);
			m_NeuralNet->set_activation_function_hidden(FANN::LINEAR_PIECE_SYMMETRIC);
			m_NeuralNet->set_activation_function_output(FANN::LINEAR_PIECE_SYMMETRIC);
			m_NeuralNet->set_learning_momentum(0.05f);
			m_NeuralNet->set_training_algorithm(FANN::TRAIN_RPROP);
			m_NeuralNet->set_train_error_function(FANN::ERRORFUNC_LINEAR);
			break;
		}

		case ENeuralPreset::Newton:
		{
			deque<UINT> params = { in };

			UINT k;
			if (in < out)
			{
				k = out - in;
				while (k>0) params.push_back(Math::BinCoef(out, k--));
			}
			else
			{
				k = in - out;
				while (k>0) params.push_back(Math::BinCoef(in, k--));
			}

			Conversions::CallVA(m_NeuralNet.get(), &FANN::neural_net::create_standard, params);
			break;
		}

		case ENeuralPreset::Custom:
		{
			m_NeuralNet = preset_custom_data;
			break;
		}
	}
	

	return false;
}

void Node::Perform(float intensity) const
{
	// Compute Current Drive.

	// Perform actions.
	auto input = prepare_input(intensity);
	for (auto& i : input)
		i *= intensity;

	float* output = m_NeuralNet->run(input.data());
}

vector<float> Node::Train(const unordered_map<string, float>& aspect_data, vector<float>& desired_drive)
{
	auto input = Conversions::AsVector<unordered_map<string, float>, float>(aspect_data);
	m_NeuralNet->train(input.data(), desired_drive.data());

	return vector<float>();
}

vector<float> Node::prepare_input(const float& intensity) const
{
	vector<float> result;
	for (const auto& a : m_Aspects)
		result.emplace_back(a.second * intensity);

	return result;
}