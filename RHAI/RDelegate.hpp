#pragma once

template<typename return_type, typename... params>
class RDelegate
{
	// Define FunctionType
	typedef return_type(*FunctionType)(void*, params...);

public:
	RDelegate() = default;

	RDelegate(void* callee, FunctionType function)
		: m_Callee(callee)
		, m_CallbackFunction(function) {}

	// The execution.
	return_type operator()(params... xs) const
	{
		return (*m_CallbackFunction)(m_Callee, xs...);
	}

	// Object calling the function
	void* m_Callee;

	// Wrapper function calling THE function on the calee
	FunctionType m_CallbackFunction;
};

template<typename T, typename return_type, typename... params>
struct DelegateMaker
{
	// Creates the delegate with the object bound to this method's WRAPPER.
	template<return_type(T::*func)(params...)>
	inline static RDelegate<return_type, params...> Bind(T* obj)
	{
		return RDelegate<return_type, params...>(obj, &DelegateMaker::MethodWrapper<func>);
	}

private:
	// Wrapping method. Calls THE method on THE object.
	template<return_type(T::*func)(params...)>
	static return_type MethodWrapper(void* obj, params... xs)
	{
		return (static_cast<T*>(obj)->*func)(xs...);
	}
};

// For all possible type combinations...
template<typename T, typename return_type, typename... params>
// Make a method that returns the matching DelegateMaker...
DelegateMaker<T, return_type, params... >
// From any member function pointer.
internal_make_delegate(return_type(T::*)(params...))
{
	return DelegateMaker<T, return_type, params...>();
}

#define MAKE_RDELEGATE(obj, func) (internal_make_delegate(func).Bind<func>(obj))