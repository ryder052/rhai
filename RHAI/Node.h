#pragma once
#include "stdafx.h"
#include "BehaviorDimension.h"
#include "BehaviorBindable.h"
#include <unordered_map>

namespace FANN
{
	class neural_net;
}

namespace RHAI
{
	enum class ENeuralPreset : BYTE
	{
		Simple,
		Newton,
		Custom
	};

	RHAI_CLASS(Node) : public INodeBindable
	{
		Node() = default;
		~Node() = default;

		void Perform(float intensity) const override;

		UINT AddDimension(pINodeBindable plus);
		UINT AddDimension(pINodeBindable plus, pINodeBindable minus);
		bool AddAspect(const string& aspect_name);
		bool Build(ENeuralPreset preset, shared_ptr<FANN::neural_net> preset_custom_data = nullptr);
		vector<float> Train(const unordered_map<string, float>& aspect_data, vector<float>& desired_drive);

		RHAI_FIELD(vector<pBehaviorDimension>, Dimensions);
		RHAI_FIELDT(unordered_map<string, float>, Aspects);
		RHAI_FIELD(vector<float>, CurrentDrive);
		RHAI_FIELD(shared_ptr<FANN::neural_net>, NeuralNet) = nullptr;

	private:
		vector<float> prepare_input(const float& intensity) const;
	};
}
