#include "ManualTests.h"
#include "RHAI.h"
#include "HelpersInternal.h"

#include <stdio.h>
#include <tchar.h>

using namespace RHAI;

struct Test
{
	static int wtf2;

	void Love(float scale)
	{
		DBG("Love %.2f\n", scale);
	}

	void Hate(float scale)
	{
		DBG("Hate %.2f\n", scale);
	}
};

void ManualTests::Run()
{
	Test test;
	DBG("%d", 333);

	pNode node = MAKE_PTR(Node);
	pNode subnode = MAKE_PTR(Node);
	auto loveBeh = MAKE_PTR(Behavior, MAKE_RDELEGATE(&test, &Test::Hate)); //make_shared<Behavior>(MAKE_RDELEGATE(&test, &Test::Hate));
	auto hateBeh = MAKE_PTR(Behavior, MAKE_RDELEGATE(&test, &Test::Hate));
	subnode->AddDimension(hateBeh);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddDimension(loveBeh, subnode);
	node->AddAspect("COOKIES");
	node->AddAspect("COOKIES2");
	node->AddAspect("COOKIES3");
	node->AddAspect("COOKIES4");
	node->AddAspect("COOKIES5");

	node->Build(ENeuralPreset::Newton);
}
