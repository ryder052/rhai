#include "EditorMain.h"
#include <QApplication>
#include "ManualTests.h"

#define USE_EDITOR 0

int main(int argc, char *argv[])
{
#if USE_EDITOR
    QApplication a(argc, argv);
    EditorMain w;
    w.show();
	return a.exec();
#else
	ManualTests::Run();
	return 0;
#endif
}
