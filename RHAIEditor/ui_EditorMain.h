/********************************************************************************
** Form generated from reading UI file 'EditorMain.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITORMAIN_H
#define UI_EDITORMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditorMain
{
public:
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *EditorMain)
    {
        if (EditorMain->objectName().isEmpty())
            EditorMain->setObjectName(QStringLiteral("EditorMain"));
        EditorMain->setEnabled(true);
        EditorMain->resize(1024, 768);
        centralWidget = new QWidget(EditorMain);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        EditorMain->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(EditorMain);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 20));
        EditorMain->setMenuBar(menuBar);
        mainToolBar = new QToolBar(EditorMain);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        EditorMain->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(EditorMain);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        EditorMain->setStatusBar(statusBar);

        retranslateUi(EditorMain);

        QMetaObject::connectSlotsByName(EditorMain);
    } // setupUi

    void retranslateUi(QMainWindow *EditorMain)
    {
        EditorMain->setWindowTitle(QApplication::translate("EditorMain", "EditorMain", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class EditorMain: public Ui_EditorMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITORMAIN_H
